package be.ucll.advancedjava.springBootOef;

import org.springframework.stereotype.Component;

@Component
public class Student {
    private int studentNummer;
    private String voorNaam;

    public Student(){

    }

    public Student(int studentNummer, String voorNaam) {
        this.studentNummer = studentNummer;
        this.voorNaam = voorNaam;

    }

    public int getStudentNummer() {
        return this.studentNummer;
    }

    public void setStudentNummer(int studentNummer) {
        this.studentNummer = studentNummer;
    }

    public String getVoorNaam() {
        return voorNaam;
    }

    public void setVoorNaam(String voorNaam) {
        this.voorNaam = voorNaam;
    }


}
