package be.ucll.advancedjava.springBootOef;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class PrintStudent implements CommandLineRunner {

    private StudentDAO studentDAO;

    @Autowired
    public PrintStudent(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    public void run(String... args) throws Exception {

        studentDAO.saveStudent(new Student(100, "ziya"));
        studentDAO.saveStudent(new Student(101, "ates"));
        studentDAO.saveStudent(new Student(102, "ahmed"));
        studentDAO.getAllStudents().forEach(x-> System.out.println(x.getVoorNaam()));
        studentDAO.updateStudent(new Student(100, "mehmet"));
        studentDAO.getAllStudents().forEach(x-> System.out.println(x.getVoorNaam()));
        studentDAO.deleteStudent(new Student(102, "ahmed"));
        studentDAO.getAllStudents().forEach(x-> System.out.println(x.getVoorNaam()));

    }
}
