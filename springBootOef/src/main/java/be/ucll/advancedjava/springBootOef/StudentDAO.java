package be.ucll.advancedjava.springBootOef;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class StudentDAO implements CommandLineRunner {

    private Connection connection;

    public static final String INSERTION_SQL = "INSERT INTO jdbc.student2 VALUES (?,?)";
    public static final String SELECT_SQL = "SELECT * FROM jdbc.student2";
    public static final String SELECT_BYID_SQL = "SELECT * FROM jdbc.student2 WHERE studentnummer = ?";
    public static final String UPDATE_SQL = "UPDATE jdbc.student2 SET voornaam = ? WHERE studentnummer = ?";
    public static final String DELETE_SQL = "DELETE FROM jdbc.student2 WHERE studentnummer = ?";

    @Autowired
    public StudentDAO(DataSource dataSource) throws SQLException {
        this.connection = dataSource.getConnection();
    }

    public Optional<Student> getStudentById(int id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BYID_SQL);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            return Optional.of(new Student(
                    resultSet.getInt("studentnummer"),
                    resultSet.getString("voornaam")
            ));
        }
        return Optional.empty();
    }

    public List<Student> getAllStudents() throws SQLException {
        ResultSet resultSet = this.connection.createStatement().executeQuery(SELECT_SQL);
        List<Student> studentList = new ArrayList<Student>();
        while (resultSet.next()) {
            studentList.add(new Student(
                    resultSet.getInt("studentnummer"),
                    resultSet.getString("voornaam")
            ));
        }
        return studentList;
    }

    public void saveStudent(Student student) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(INSERTION_SQL);
        preparedStatement.setInt(1, student.getStudentNummer());
        preparedStatement.setString(2, student.getVoorNaam());
        preparedStatement.executeUpdate();
    }

    public void updateStudent(Student student) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
        preparedStatement.setString(1, student.getVoorNaam());
        preparedStatement.setInt(2, student.getStudentNummer());
        int checkUpdated = preparedStatement.executeUpdate();
    }

    public void deleteStudent(Student student) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);
        preparedStatement.setInt(1, student.getStudentNummer());
        int checkDeleted = preparedStatement.executeUpdate();
    }


    @Override
    public void run(String... args) throws Exception {
        getAllStudents().forEach(System.out::println);
    }
}
