package be.ucll.advancedjava.springBootOef;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
class SpringBootOefApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringBootOefApplication.class, args);
	}

}
